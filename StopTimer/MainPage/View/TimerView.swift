//
//  ContentView.swift
//  StopTimer
//
//  Created by Vladislav on 27.04.21.
//

import SwiftUI

struct TimerView: View {
    @ObservedObject var viewModel = TimerViewModel()
    @SwiftUI.State var showFireworks: Bool = false
    @SwiftUI.State var finishFireworks: Bool = false

    var body: some View {
        ZStack {
            NavigationView {
                VStack(alignment: .center, spacing: 20, content: {
                    Text(viewModel.elapsedTimeString)
                        .font(.system(size: 100, weight: .regular, design: .default))
                    
                    LazyVStack(alignment: .center, spacing: 20, content: {
                        ForEach(viewModel.results, id: \.self) { result in
                            if let timeResult = result.timeResult {
                                HStack(spacing: 50, content: {
                                    Text(result.time)
                                    Text(timeResult)
                                })
                                
                            } else {
                                Text(result.time)
                            }
                        }
                        
                        HStack {
                            Text("Результат: ")
                            Text(viewModel.scoreString)
                        }.padding()
                    })
                    .font(.system(size: 25, weight: .regular, design: .monospaced))
                    .padding()
                    
            
                    Button(viewModel.textButton) {
                        self.viewModel.updateButtonState()
                        if viewModel.showBestResultAnimation {
                            bestResultAnimation()
                        }
                    }
                    .frame(width: 120, height: 120)
                    .foregroundColor(Color.white)
                    .background(Color.green)
                    .clipShape(Circle())
                    .font(.title)
                    .disabled(showFireworks)
                })
            }
            
            FireworksView()
                .scaleEffect(showFireworks ? 1 : 0, anchor: .top)
                .opacity(showFireworks && !finishFireworks ? 1 : 0)
                .offset(y: showFireworks ? 0 : UIScreen.main.bounds.height / 2)
                .ignoresSafeArea()
        }
    }
    

    func bestResultAnimation() {
        withAnimation(.spring()) {
            showFireworks = true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            withAnimation(.easeInOut(duration: 1.5)) {
                finishFireworks = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                finishFireworks = false
                showFireworks = false
            }
        }
    }
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView()
    }
}
