//
//  ImagePicker.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 10.07.21.
//

import SwiftUI
import PhotosUI

struct ImagePicker : UIViewControllerRepresentable {
    @SwiftUI.Binding var picker: Bool
    @SwiftUI.Binding var imageData: Data
    @SwiftUI.State var action: () -> ()
    
    func makeCoordinator() -> Coordinator {
        return ImagePicker.Coordinator(parent: self)
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        var config = PHPickerConfiguration()
        config.selectionLimit = 1
        
        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        var parent: ImagePicker
        
        init(parent: ImagePicker) {
            self.parent = parent
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            if results.isEmpty {
                self.parent.picker.toggle()
                return
            }
            
            if let item = results.first?.itemProvider {
                if item.canLoadObject(ofClass: UIImage.self) {
                    item.loadObject(ofClass: UIImage.self, completionHandler: { image, error in
                        if error != nil { return }
                        
                        let imageData = image as! UIImage
                        
                        DispatchQueue.main.async {
                            self.parent.imageData = imageData.jpegData(compressionQuality: 0.5)!
                            self.parent.picker.toggle()
                            self.parent.action()
                        }
                    })
                }
            }
        }
    }
}
