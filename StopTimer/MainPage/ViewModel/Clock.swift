//
//  Timer.swift
//  StopTimer
//
//  Created by Vladislav on 27.04.21.
//

import Foundation
import Combine

enum State {
    case notRunning
    case running
}

final class Clock {
    @Published var timeElapsed = 0
    private let timeInterval: TimeInterval
    private var cancellableTimer: AnyCancellable!

    required init(timerInterval: TimeInterval) {
        self.timeInterval = timerInterval
    }
    
    func start() {
        cancellableTimer = Timer.publish(every: timeInterval, on: RunLoop.main, in: .default)
        .autoconnect()
        .sink(receiveValue: { (_) in
            self.timeElapsed += 1
        })
    }
    
    func stop() {
        timeElapsed = 0
        cancellableTimer?.cancel()
    }
}
