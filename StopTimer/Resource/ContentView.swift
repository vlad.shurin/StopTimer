//
//  ContentView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 10.07.21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            TimerView().tabItem {
                Image(systemName: "gamecontroller")
                Text("Таймер")
            }
            
            InfoView().tabItem {
                Image(systemName: "person.circle.fill")
                Text("Инфо")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
