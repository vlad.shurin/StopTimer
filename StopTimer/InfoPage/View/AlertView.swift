//
//  AlertView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 19.07.21.
//

import SwiftUI

func alertView(msg: String, completion: @escaping (String) -> ()) {
    let alert = UIAlertController(title: "Сообщение", message: msg, preferredStyle: .alert)
    
    alert.addTextField { (txt) in
        txt.placeholder = "Текст"
    }
    
    alert.addAction(UIAlertAction(title: "Отмена", style: .destructive))
    alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { _ in
        if let textFields = alert.textFields {
            completion(textFields[0].text ?? "")
        }
    }))
    
    UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
}
