//
//  EmitterView.swift
//  StopTimer
//
//  Created by Vladislav on 1.06.21.
//

import SwiftUI

struct FireworksView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        view.backgroundColor = .clear
        
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = createEmitterCells()
        emitterLayer.emitterSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
        emitterLayer.emitterPosition = CGPoint(x: UIScreen.main.bounds.width / 2, y: 0)
       
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        
    }
    
    func createEmitterCells() -> [CAEmitterCell] {
        var cells: [CAEmitterCell] = []
        
        for index in 1...12 {
            let cell = CAEmitterCell()
            cell.contents = UIImage(named: getImage(index: index))?.cgImage
            cell.color = getColor().cgColor
            cell.birthRate = 4.5
            cell.lifetime = 20
            cell.velocity = 120
            cell.scale = 0.2
            cell.scaleRange = 0.3
            cell.emissionLongitude = .pi
            cell.emissionRange = 0.5
            cell.spin = 3.5
            cell.spinRange = 1
            cell.yAcceleration = 40
            cells.append(cell)
        }
        
        return cells
    }
    
    func getColor() -> UIColor {
        let colors: [UIColor] = [.systemPink, .systemGreen, .systemBlue, .systemRed, .systemOrange, .systemYellow, .systemPurple]
        return colors.randomElement()!
    }
    
    func getImage(index: Int) -> String {
        if index < 4 {
            return "circle"
        } else if index >= 5 && index <= 8 {
            return "rectangle"
        } else {
            return "triengle"
        }
    }
}
