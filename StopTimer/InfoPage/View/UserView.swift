//
//  UserView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 9.07.21.
//

import SwiftUI
import SDWebImageSwiftUI

struct UserView: View {

    @ObservedObject var viewModel = UserViewModel()

    var body: some View {
        VStack {
            
            ZStack {
                if let image = viewModel.userInfo?.image {
                    WebImage(url: URL(string: image)!)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 150, height: 150)
                        .clipShape(Circle())
                } else {
                    Image(systemName: "person")
                        .font(.system(size: 65))
                        .foregroundColor(.black)
                        .frame(width: 150, height: 150)
                        .background(Color.white)
                        .clipShape(Circle())
                }
            }
            .padding(.top)
            .onTapGesture {
                viewModel.picker.toggle()
            }
            
            if let userInfo = viewModel.userInfo {
                HStack {
                    Text(userInfo.nickName ?? "Псевдоним")
                        .font(.title2)
                        .fontWeight(.bold)
                        .foregroundColor(userInfo.nickName == nil ? .gray : .accentColor)
                    
                    Button(action: {
                        self.viewModel.updateNickName()
                    }, label: {
                        Image(systemName: "pencil.circle.fill")
                            .font(.system(size: 24))
                    })
                }
                .padding(.top, 20)
                
                Text(userInfo.email)
                    .padding(.top, 10)
                
                if let bestResult = userInfo.bestResult {
                    Text("Лучший результат: " + TimeConverter.millesecondsToTime(milliseconds: bestResult))
                        .fontWeight(.bold)
                        .padding(.top, 5)
                }
            }
            
            Button(action: {
                self.viewModel.logout()
            }, label: {
                Text("Выход")
                    .foregroundColor(.white)
                    .fontWeight(.bold)
                    .padding(.vertical)
                    .frame(width: UIScreen.main.bounds.width - 100)
                    .background(Color("Red"))
                    .clipShape(Capsule())
            })
            .padding(.top, 20)
        }
        .sheet(isPresented: $viewModel.picker, content: {
            ImagePicker(picker: $viewModel.picker, imageData: $viewModel.imageData, action: {
                viewModel.saveImage()
            })
        })
    }
}
