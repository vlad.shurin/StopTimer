//
//  ErrorView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 7.07.21.
//

import SwiftUI

struct ErrorView: View {
    @SwiftUI.State var color = Color.black.opacity(0.7)
    @SwiftUI.State var action: () -> ()
    @SwiftUI.State var text: String
    @SwiftUI.State var header: String

    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .center) {
                HStack {
                    Text(header)
                        .font(.title)
                        .foregroundColor(self.color)
                        .fontWeight(.bold)
                    Spacer()
                }
                .padding(.horizontal, 25)
                
                
                Text(self.text)
                    .foregroundColor(self.color)
                    .padding(.top)
                    .padding(.horizontal, 25)
                
                Button(action: {
                    self.action()
                }, label: {
                    Text("Закрыть")
                        .foregroundColor(.white)
                        .frame(width: UIScreen.main.bounds.width - 120)
                        .padding(.vertical)
                })
                .background(Color("Red"))
                .cornerRadius(10)
                .padding(.top, 25)
            }
            .padding(.vertical, 25)
            .background(Color.white)
            .frame(width: UIScreen.main.bounds.width - 70)
            .cornerRadius(15)
            .position(x: geometry.size.width / 2, y: geometry.size.height / 2)
        }
        .background(Color.black.opacity(0.3).edgesIgnoringSafeArea(.all))
    }
}
