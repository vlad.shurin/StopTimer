//
//  InfoViewModel.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 7.07.21.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    @Published var alert: Bool = false
    @Published var textAlert: String = ""
    @Published var alertHeader: String = ""
    private let firebaseManager = FirebaseManager()

    func logIn(email: String, password: String) {
        if email != "" && password != "" {
            firebaseManager.logIn(email: email, password: password) { status, errorMessage in
                if status == .ERROR {
                    self.textAlert = errorMessage ?? ""
                    self.alertHeader = "Ошибка"
                    self.alert.toggle()
                    return
                }
                
                UserDefaults.standard.set(true, forKey: "AuthStatus")
                NotificationCenter.default.post(name: NSNotification.Name("AuthStatus"), object: nil)
            }
        } else {
            self.textAlert = "Невернные данные"
            self.alertHeader = "Ошибка"
            self.alert.toggle()
        }
    }
        
    func resetPassword(email: String) {
        if email == "" {
            self.textAlert = "Заполните поле Email"
            self.alertHeader = "Ошибка"
            self.alert.toggle()
            return
        }
        
        firebaseManager.resetPassword(email: email) { status, error in
            if status == .ERROR {
                self.textAlert = error ?? ""
                self.alertHeader = "Ошибка"
                self.alert.toggle()
                return
            }
            
            self.textAlert = "Пароль успешно сброшен. Cсылка отправлена на почту"
            self.alertHeader = "Сообщение"
            self.alert.toggle()
        }
    }
    
    func closeError() {
        self.alert.toggle()
    }
}
