//
//  StatusCode.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 20.07.21.
//

import Foundation

enum StatusCode {
    case OK
    case ERROR
}
