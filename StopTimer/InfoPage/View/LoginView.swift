//
//  AuthView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 7.07.21.
//

import SwiftUI

struct LoginView: View {
    @SwiftUI.State var email = ""
    @SwiftUI.State var password = ""
    @SwiftUI.State var visible = false
    @SwiftUI.Binding var show: Bool
    
    @ObservedObject var viewModel = LoginViewModel()


    var body: some View {
        ZStack {
            ZStack(alignment: .topTrailing) {
                GeometryReader { geometry in
                    VStack {
                        Image("timer")
                        
                        Text("Авторизация")
                            .font(.title)
                            .fontWeight(.bold)
                            .padding(.top, 25)
                        
                        TextField("Логин", text: self.$email)
                            .autocapitalization(.none)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.email == "" ? Color("Red") : Color.green.opacity(0.7), lineWidth: 2))
                            .padding(.top, 25)
                        
                        HStack(spacing: 15) {
                            VStack {
                                if self.visible {
                                    TextField("Пароль", text: self.$password)
                                        .autocapitalization(.none)
                                } else {
                                    SecureField("Пароль", text: self.$password)
                                        .autocapitalization(.none)
                                }
                            }
                            
                            Button(action: {
                                self.visible.toggle()
                            }, label: {
                                Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                            })
                        }
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 4).stroke(self.password == "" ? Color("Red") : Color.green.opacity(0.7), lineWidth: 2))
                        .padding(.top, 25)
                        
                        HStack {
                            Spacer()
                            
                            Button(action: {
                                self.viewModel.resetPassword(email: self.email)
                            }, label: {
                                Text("Забыли пароль")
                                    .foregroundColor(Color("Red"))
                            })
                        }
                        .padding(.top, 15)
                        
                        Button(action: {
                            viewModel.logIn(email: self.email, password: self.password)
                        }, label: {
                            Text("Войти")
                                .foregroundColor(.white)
                                .padding(.vertical)
                                .frame(width: UIScreen.main.bounds.width - 50)
                        })
                        .background(Color("Red"))
                        .cornerRadius(10.0)
                        .padding(.top, 25)
                    }
                    .padding(.horizontal, 15)
                    .position(x: geometry.size.width / 2, y: geometry.size.height / 2)
                }
                
                
                Button(action: {
                    self.show.toggle()
                }, label: {
                    Text("Регистрация")
                        .foregroundColor(Color("Red"))
                        .fontWeight(.bold)
                })
                .padding()
            }.onTapGesture {
                UIApplication.shared.endEditing()
            }

            if self.viewModel.alert {
                ErrorView(action: {
                    self.viewModel.closeError()
                }, text: self.viewModel.textAlert, header: self.viewModel.alertHeader)
            }
        }
    }
}
