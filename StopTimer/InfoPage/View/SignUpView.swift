//
//  RegisterView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 7.07.21.
//

import SwiftUI

struct SignUpView: View {
    @SwiftUI.State var email = ""
    @SwiftUI.State var password = ""
    @SwiftUI.State var repeatPassword = ""
    @SwiftUI.State var visible = false
    @SwiftUI.State var repeatVisible = false
    @SwiftUI.Binding var show: Bool
    
    @ObservedObject var viewModel = SignUpViewModel()
    
    var body: some View {
        ZStack {
            ZStack(alignment: .topLeading) {
                GeometryReader { geometry in
                    VStack {
                        Image("timer")
                        
                        Text("Регистрация")
                            .font(.title)
                            .fontWeight(.bold)
                            .padding(.top, 25)
                        
                        TextField("Логин", text: self.$email)
                            .autocapitalization(.none)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 4).stroke(self.email == "" ? Color("Red") : Color.green.opacity(0.7), lineWidth: 2))
                            .padding(.top, 25)
                        
                        HStack(spacing: 15) {
                            VStack {
                                if self.visible {
                                    TextField("Пароль", text: self.$password)
                                        .autocapitalization(.none)
                                } else {
                                    SecureField("Пароль", text: self.$password)
                                        .autocapitalization(.none)
                                }
                            }
                            
                            Button(action: {
                                self.visible.toggle()
                            }, label: {
                                Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                            })
                        }
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 4).stroke(self.password == "" ? Color("Red") : Color.green.opacity(0.7), lineWidth: 2))
                        .padding(.top, 25)
                        
                        HStack(spacing: 15) {
                            VStack {
                                if self.repeatVisible {
                                    TextField("Повторите пароль", text: self.$repeatPassword)
                                        .autocapitalization(.none)
                                } else {
                                    SecureField("Повторите пароль", text: self.$repeatPassword)
                                        .autocapitalization(.none)
                                }
                            }
                            
                            Button(action: {
                                self.repeatVisible.toggle()
                            }, label: {
                                Image(systemName: self.repeatVisible ? "eye.slash.fill" : "eye.fill")
                            })
                        }
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 4).stroke(self.repeatPassword == "" ? Color("Red") : Color.green.opacity(0.7), lineWidth: 2))
                        .padding(.top, 25)
                        
                        Button(action: {
                            self.viewModel.register(email: self.email, password: self.password, repeatPassword: self.repeatPassword)
                        }, label: {
                            Text("Зарегестрироваться")
                                .foregroundColor(.white)
                                .padding(.vertical)
                                .frame(width: UIScreen.main.bounds.width - 50)
                        })
                        .background(Color("Red"))
                        .cornerRadius(10.0)
                        .padding(.top, 25)
                    }
                    .padding(.horizontal, 25)
                    .position(x: geometry.size.width / 2, y: geometry.size.height / 2)
                }.onTapGesture {
                    UIApplication.shared.endEditing()
                }
                
                
                Button(action: {
                    self.show.toggle()
                }, label: {
                    Image(systemName: "chevron.left")
                        .font(.title)
                        .foregroundColor(Color("Red"))
                })
                .padding()
            }
            
            if self.viewModel.alert {
                if self.viewModel.alert {
                    ErrorView(action: {
                        self.viewModel.closeError()
                    }, text: self.viewModel.textAlert, header: self.viewModel.alertHeader)
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}
