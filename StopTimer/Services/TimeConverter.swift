//
//  TimeConverter.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 20.07.21.
//

import Foundation

class TimeConverter {
    static func millesecondsToTime(milliseconds: Int) -> String {
        let ms = milliseconds % 100
        let seconds = milliseconds / 100
        return String(format: "%0.2d:%0.2d", seconds, ms)
    }
}
