//
//  ResultGame.swift
//  StopTimer
//
//  Created by Vladislav on 28.04.21.
//

import Foundation

struct ResultGame: Hashable {
    var time: String
    var milliseconds: Int
    var timeResult: String?
    var millisecondsResult: Int?
}
