//
//  SignUpViewModel.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 10.07.21.
//

import Foundation
import Combine

class SignUpViewModel: ObservableObject {
    @Published var alert: Bool = false
    @Published var textAlert: String = ""
    @Published var alertHeader: String = ""
    private let firebaseManager = FirebaseManager()

    func register(email: String, password: String, repeatPassword: String) {
        if email == "" {
            self.textAlert = "Заполните поле логин"
            self.alertHeader = "Ошибка"
            self.alert.toggle()
            return
        }
        
        if password == "" || repeatPassword == "" {
            self.textAlert = "Заполните поле пароль"
            self.alertHeader = "Ошибка"
            self.alert.toggle()
            return
        }
        
        if password != repeatPassword {
            self.textAlert = "Пароли не совпадают"
            self.alertHeader = "Ошибка"
            self.alert.toggle()
            return
        }
        
        firebaseManager.registerUser(email: email, password: password) { status, error in
            if status == .ERROR {
                self.textAlert = error ?? ""
                self.alertHeader = "Ошибка"
                self.alert.toggle()
                return
            }
            
            UserDefaults.standard.set(true, forKey: "AuthStatus")
            NotificationCenter.default.post(name: NSNotification.Name("AuthStatus"), object: nil)
        }
        
    }
    
    func closeError() {
        self.alert.toggle()
    }
}
