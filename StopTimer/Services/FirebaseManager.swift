//
//  FirebaseService.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 20.07.21.
//

import Firebase

class FirebaseManager {
    let ref = Firestore.firestore()
    
    func logIn(email: String, password: String, completion: @escaping (StatusCode, String?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { result, er in
            if let errorMessage = er {
                completion(.ERROR, errorMessage.localizedDescription)
                return
            }
            
            self.updateLocalBestResult(uid: Auth.auth().currentUser?.uid)
            
            completion(.OK, nil)
        }
    }
    
    func updateLocalBestResult(uid: String?) {
        if let uid = uid {
            ref.collection("Users").document(uid).getDocument { doc, error in
                guard let user = doc else { return }
                UserDefaults.standard.setValue(user.data()?["bestResult"] ?? nil, forKey: "bestResult")
            }
        }
    }
    
    func resetPassword(email: String, completion: @escaping (StatusCode, String?) -> ()) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if let errorMessage = error {
                completion(.ERROR, errorMessage.localizedDescription)
                return
            }
            
            completion(.OK, nil)
        }
    }
    
    func registerUser(email: String, password: String, completion: @escaping (StatusCode, String?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if error != nil {
                completion(.ERROR, error?.localizedDescription)
                return
            }
            
            if let uid = result?.user.uid {
                let data = ["uid": uid, "imageUrl": nil, "bestResult": UserDefaults.standard.value(forKey: "bestResult"), "nickName": nil ] as [String : Any]
                self.ref.collection("Users").document(uid).setData(data)
            }
            
            completion(.OK, nil)
        }
    }
    
    func fetchUser(completion: @escaping (StatusCode, String?, UserModel?) -> ()) {
        let uid = Auth.auth().currentUser?.uid
        if let uid = uid {
            ref.collection("Users").document(uid).getDocument { doc, error in
                guard let user = doc else {
                    completion(.ERROR, "Пользователь не найден", nil)
                    return
                }
                let bestResult = user.data()?["bestResult"] as? Int
                let userId = user.data()?["uid"] as? String
                let image = user.data()?["imageUrl"] as? String
                let nickName = user.data()?["nickName"] as? String
                completion(.OK, nil, UserModel(
                            bestResult: bestResult,
                            uid: userId ?? "",
                            image: image,
                            email: Auth.auth().currentUser?.email ?? "",
                            nickName: nickName))
            }
        }
    }
    
    func uploadImage(imageData: Data, completion: @escaping (StatusCode, String?, String?) -> ()) {
        let storage = Storage.storage().reference()
        let uid = Auth.auth().currentUser?.uid
        if let uid = uid {
            storage.child("profile_photo").child(uid).putData(imageData, metadata: nil) { (_, err) in
                if err != nil {
                    completion(.ERROR, err?.localizedDescription, nil)
                    return
                }
                
                storage.child("profile_photo").child(uid).downloadURL { (url, err) in
                    if err != nil {
                        completion(.ERROR, err?.localizedDescription, nil)
                        return
                    }
                    
                    completion(.OK, nil, url?.description)
                }
            }
        }
    }
    
    func updateField(field: String, value: Any, completion: @escaping (StatusCode, String?) -> ()) {
        let uid = Auth.auth().currentUser?.uid
        if let uid = uid {
            self.ref.collection("Users").document(uid).updateData([field: value]) { (err) in
                if err != nil {
                    completion(.ERROR, err?.localizedDescription)
                    print("ERROR \(err?.localizedDescription)")
                    return
                }
                
                print("SAVE \(field) : \(value)")
                completion(.OK, nil)
            }
        }
    }
    
    func logout() {
        try! Auth.auth().signOut()
    }
}
