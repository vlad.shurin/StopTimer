//
//  LoginView.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 7.07.21.
//

import SwiftUI

struct InfoView: View {
    @SwiftUI.State var show = false
    @SwiftUI.State var statusAuth = UserDefaults.standard.value(forKey: "AuthStatus") as? Bool ?? false

    var body: some View {
        NavigationView {
            
            VStack {
                if statusAuth {
                    UserView()
                } else {
                    ZStack {
                        NavigationLink(destination: SignUpView(show: self.$show), isActive: self.$show) {
                            Text("")
                        }
                        .hidden()
                        
                        LoginView(show: self.$show)
                    }
                }
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .onAppear {
                NotificationCenter.default.addObserver(forName: NSNotification.Name("AuthStatus"), object: nil, queue: .main) { _ in
                    self.statusAuth = UserDefaults.standard.value(forKey: "AuthStatus") as? Bool ?? false
                }
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
