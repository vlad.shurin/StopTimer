//
//  TimerViewModel.swift
//  StopTimer
//
//  Created by Vladislav on 27.04.21.
//

import Foundation
import Combine

class TimerViewModel: ObservableObject {
    @Published private(set) var showBestResultAnimation: Bool = false
    @Published private(set) var elapsedTimeString: String = "00:00"
    @Published private(set) var scoreString: String = "00:00"
    @Published private(set) var textButton: String = "СТАРТ"
    @Published private(set) var results: [ResultGame] = [
        ResultGame(time: "02:00", milliseconds: 200),
        ResultGame(time: "04:00", milliseconds: 400),
        ResultGame(time: "06:00", milliseconds: 600),
        ResultGame(time: "08:00", milliseconds: 800),
        ResultGame(time: "10:00", milliseconds: 1000)
    ]

    private let firebaseManager = FirebaseManager()
    private var cancellable: AnyCancellable!
    private var clock = Clock(timerInterval: 0.01)
    private var tapCounter = 0
    
    private(set) var elapsedTime: Int = 0 {
        didSet {
            checkTime()
            elapsedTimeString = TimeConverter.millesecondsToTime(milliseconds: elapsedTime)
        }
    }
    private(set) var score: Int = 0 {
        didSet {
            scoreString = TimeConverter.millesecondsToTime(milliseconds: score)
        }
    }

    private var state: State  = .notRunning {
        didSet {
            switch state {
            case .running:
                resetData()
                clock.stop()
                clock.start()
                textButton = "СТОП"
            case .notRunning:
                clock.stop()
                textButton = "СТАРТ"
            }
        }
    }
    
    init() {
        cancellable = clock.$timeElapsed
            .map({ $0 })
            .assign(to: \TimerViewModel.elapsedTime, on: self)
    }
    
    private func checkClick() {
        let result = results[tapCounter].milliseconds - elapsedTime
        results[tapCounter].millisecondsResult = result
        results[tapCounter].timeResult = (result < 0 ? "+" : "-") + TimeConverter.millesecondsToTime(milliseconds: abs(result))
        score += abs(result)
    }
    
    private func resetData() {
        score = 0
        tapCounter = 0
        results = [
            ResultGame(time: "02:00", milliseconds: 200),
            ResultGame(time: "04:00", milliseconds: 400),
            ResultGame(time: "06:00", milliseconds: 600),
            ResultGame(time: "08:00", milliseconds: 800),
            ResultGame(time: "10:00", milliseconds: 1000)
        ]
    }
    
    private func checkTime() {
        switch elapsedTime {
        case let time where time == 400 && tapCounter == 0:
            updateButtonState()
        case let time where time == 600 && tapCounter == 1:
            updateButtonState()
        case let time where time == 800 && tapCounter == 2:
            updateButtonState()
        case let time where time == 1000 && tapCounter == 3:
            updateButtonState()
        case let time where time == 1200 && tapCounter == 4:
            updateButtonState()
        default:
            break
        }
    }
    
    
    func updateButtonState() {
        if (state == .notRunning) {
            state = .running
            showBestResultAnimation = false
        } else {
            checkClick()
            tapCounter += 1
            if tapCounter == 5 {
                state = .notRunning
                checkResult()
                
            }
        }
    }
    
    func checkResult() {
        if let oldResult = UserDefaults.standard.value(forKey: "bestResult") as? Int {
            if oldResult < score {
                return
            }
        }
        showBestResultAnimation = true
        UserDefaults.standard.setValue(score, forKey: "bestResult")
        
        print(score)
        firebaseManager.updateField(field: "bestResult", value: score) { status, error in
            if status == .OK {
                NotificationCenter.default.post(name: NSNotification.Name("UpdateBestResult"), object: nil)
            }
        }
    }
}
