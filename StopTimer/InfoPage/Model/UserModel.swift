//
//  UserModel.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 19.07.21.
//

import Foundation

struct UserModel {
    var bestResult: Int?
    var uid: String
    var image: String?
    var email: String
    var nickName: String?
}
