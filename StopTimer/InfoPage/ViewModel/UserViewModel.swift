//
//  UserViewModel.swift
//  StopTimer
//
//  Created by ULADZISLAU SHURYN on 9.07.21.
//

import Foundation
import Combine

class UserViewModel: ObservableObject {
    @Published var imageData = Data(count: 0)
    @Published var picker = false
    @Published var userInfo: UserModel? = nil
    
    private let firebaseManager = FirebaseManager()

    init() {
        fetchUser()
        NotificationCenter.default.addObserver(forName: NSNotification.Name("UpdateBestResult"), object: nil, queue: .main) { _ in
            self.fetchUser()
        }
    }
    
    func fetchUser() {
        firebaseManager.fetchUser { [weak self] status, error, userData in
            if status == .OK {
                DispatchQueue.main.async {
                    self?.userInfo = userData
                }
            }
        }
    }
    
    func saveImage() {
        firebaseManager.uploadImage(imageData: imageData) { [weak self] status, error, url in
            if status == .OK {
                self?.firebaseManager.updateField(field: "imageUrl", value: url) { [weak self] status, error in
                    if status == .OK {
                        DispatchQueue.main.async {
                            self?.userInfo?.image = url
                        }
                    }
                }
            }
        }
    }
    
    func updateNickName() {
        alertView(msg: "Изменение псевдонима") { [weak self] nickName in
            if nickName != "" {
                self?.firebaseManager.updateField(field: "nickName", value: nickName) { [weak self] status, error in
                    if status == .OK {
                        DispatchQueue.main.async {
                            self?.userInfo?.nickName = nickName
                        }
                    }
                }
            }
        }
    }
    
    func logout() {
        firebaseManager.logout()
        UserDefaults.standard.set(false, forKey: "AuthStatus")
        NotificationCenter.default.post(name: NSNotification.Name("AuthStatus"), object: nil)
    }
}
